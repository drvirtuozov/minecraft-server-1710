"use strict";

const net = require("net");
const WebSocketServer = require("ws").Server;
const wss = new WebSocketServer({ port: 8081 }, () => {
  console.log("Websocket server is listening on 8081.");
});


wss.on("connection", ws => {
  let minecraft = net.connect({ port: 25565 }, () => {
    console.log("Connected to Minecraft server!");
  });
  
  minecraft.on("data", data => {
    ws.send(data);
  });
  
  minecraft.on("close", () => {
    console.log("tcp close");
    console.log('Disconnected from Minecraft server.');
    ws.close();
  });
  
  ws.on("close", () => {
    console.log("ws closed");
    minecraft.destroy();
  });
  
  ws.on("message", message => {
    minecraft.write(message);
  });
});